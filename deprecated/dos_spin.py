import matplotlib.pyplot as plt
import numpy as np

def pdos_spin(flnm,ftitle):
    vct = []
    en = []
    dos_up = []
    dos_dn = []
    #intdos = []
    ef = -1e20
    enmin = 1e20
    enmax = -1e20
    qbreak = 0
    cnt = 0
    apgap_up = -1e20
    apgap_dn = -1e20
    try:
        with open(flnm,'r') as dosin:
            for ln,ctt in enumerate(dosin):
                if cnt == 0:
                    if ln == 0:
                        tmp = [float(tt) for tt in ctt.split()]
                        nion = tmp[0]
                    elif ln == 1:
                        tmp = [float(tt) for tt in ctt.split()]
                        wsvol = tmp[0]
                    elif 1 < ln < 5:
                        continue
                    elif ln==5:
                        vct = [float(tt) for tt in ctt.split()]
                        enmax = vct[0]
                        enmin = vct[1]
                        ef = vct[3]
                        cnt += 1
                elif cnt == 1:
                    tmp = [float(tt) for tt in ctt.split()]
                    if len(tmp) == 5 and abs(tmp[0]-enmax) < 1e-8:
                        cnt += 1
                        continue
                    elif len(tmp) == 5:
                        en.append(tmp[0]-ef)
                        dos_up.append( tmp[1]/wsvol)
                        dos_dn.append( tmp[2]/wsvol)
                        #intdos.append( tmp[2])
                elif cnt == 2:
                    break

        for ien,uen in enumerate(en):
            if uen > 0.0:
                break

        if dos_up[ien] > 0.0:
            apgap_up = 0.0
        else:
            for jen in range(ien,len(en)-1):
                if dos_up[jen] > 0.0:
                    break
            apgap_up = (en[jen] + en[jen-1])/2.0

        if dos_dn[ien] > 0.0:
            apgap_dn = 0.0
        else:
            for jen in range(ien,len(en)-1):
                if dos_dn[jen] > 0.0:
                    break
            apgap_dn = (en[jen] + en[jen-1])/2.0

        return ef,apgap_up,apgap_dn
        
    except IOError:
        print('DOSCAR file not present')
        exit()
