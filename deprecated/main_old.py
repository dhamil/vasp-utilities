import ase.io.vasp
import ase.build
import plot_n
import read_pot
import gap
import plot_dos
import os.path
import time
from os import getcwd,sys,system,remove


# Utilities for VASP
# written by Aaron Kaplan
# last modification, 6 February, 2020

def make_sup_cell(nx,ny,nz,fname,qnat=True):
    try:
        cell = ase.io.vasp.read_vasp("CONTCAR")
        if qnat:
            tmx = ase.build.find_optimal_cell_shape(cell.cell,nx,'sc')
            spcell = ase.build.make_supercell(cell,tmx)
        else:
            spcell=cell*(nx,ny,nz)
        ase.io.vasp.write_vasp(fname,spcell, label='spcell',direct=False,sort=True)
        return
    except IOError:
        print("CONTCAR unavailable")
        return

def get_paw_core():
    a0 = 0.529177210903 # Bohr radius from NIST, needed for PAW core radius
    if os.path.isfile('OUTCAR') and os.path.getsize('OUTCAR')>0:
        system("grep 'RCORE' OUTCAR > tmpflks.txt")
    elif os.path.isfile('POTCAR') and os.path.getsize('POTCAR')>0:
        system("grep 'RCORE' POTCAR > tmpflks.txt")
    else:
        print('Error: need OUTCAR or POTCAR to get PAW core radius.')
        raise IOError
    l = []
    with open('tmpflks.txt','r') as tmpfl:
        for u in tmpfl:
            l.append(u.split())
    remove('tmpflks.txt')

    return [float(l[i][2])*a0 for i in range(len(l))]


if __name__=='__main__':

    print("------------------------------------------------------------")
    print('||                VASP utilities                          ||')
    print('||            written by Aaron Kaplan                     ||')
    print('||      last modification,',time.ctime(os.path.getmtime(__file__)),'      ||')
    print("------------------------------------------------------------")

    gtho = False
    sux = False

    while True: # Main menu loop
        print( '[1] gap       ---->   Find gap from EIGENVAL')
        print( '[2] kspot     ---->   Write or plot KS potential to file from LOCPOT')
        print( '[3] dens      ---->   Write density to file from CHG')
        print( '[4] dos       ---->   Plot density of states from DOSCAR')
        print( '[5] spc       ---->   Make a supercell from CONTCAR')
        print( '[6] exit      ---->   Exit')


        main_inp = input()
        try : # option selection loop
            if main_inp == "gap" or main_inp == '1':
                if len(sys.argv) > 1:
                    infname = sys.argv[1]
                else:
                    infname = 'EIGENVAL'
                gap.gap(infname)
                sux = True
            elif main_inp == "kspot" or main_inp == '2':
                while True:
                    print('[1] full    ---->  write full KS potential to file from LOCPOT')
                    print('[1b] multi  ---->  write the forbidden region volume from multiple LOCPOT files prefixed as "locpot_"')
                    print('[2] cviol   ---->  only write regions where eps_HO < vs(r_vec)')
                    print('[3] mm      ---->  Return to main menu')
                    pot_inp = input()
                    try:
                        if pot_inp == 'full' or pot_inp == '1':
                            epsho=gap.gap(qone =True)
                            pcr = get_paw_core()
                            read_pot.wpot(epsho,pcr,qone =True)
                            sux = True
                            break
                        elif pot_inp == 'multi' or pot_inp == '1b':
                            epsho,gapl=gap.gap(qone=False)
                            pcr = get_paw_core()
                            read_pot.wpot(epsho,gapl,pcr,qone =False)
                            sux = True
                            break
                        elif pot_inp == 'cviol' or pot_inp == '2':
                            if os.path.isfile('ks_pot.csv') and os.path.getsize('ks_pot.csv') > 0:
                                read_pot.w_cviol()
                                sux = True
                                break
                            else:
                                epsho,gapl=gap.gap(qone =True)
                                pcr = get_paw_core()
                                read_pot.wpot(epsho,gapl,pcr,qone =True)
                                read_pot.w_cviol()
                                sux = True
                                break
                        elif pot_inp == 'mm' or pot_inp == '3':
                            break
                    except ValueError:
                        print('Not a recognized option')

            elif main_inp == "dens" or main_inp == '3':
                while True:
                    print('[1] full  ---->  Full CHG file')
                    print('[2] 110   ---->  Along 110 surface?')
                    print('[3] mm    ---->  Return to main menu')
                    dens_inp = input()
                    try:
                        if dens_inp == 'full' or dens_inp == '1':
                            pcr = get_paw_core()
                            plot_n.write_dens(pcr,q110=False)
                            sux = True
                            break
                        elif dens_inp == '110' or dens_inp == '2':
                            pcr = get_paw_core()
                            plot_n.write_dens(pcr,q110=True)
                            sux = True
                            break
                        elif dens_inp == 'mm' or dens_inp == '3':
                            break
                    except ValueError:
                        print('Not a recognized option')
            elif main_inp == 'dos' or main_inp == '4':
                print('Enter plot title')
                tit_inp = input()
                plot_dos.pdos(tit_inp)
                sux = True
            elif main_inp == 'spc' or main_inp == '5':
                while True:
                    print('[1] ion  ----> specify number of ion sites')
                    print('[2] dim  ----> specify linear scale factors')
                    print('[3] mm   ----> return to main menu')
                    sc_in = input()
                    try:
                        if sc_in == 'ion' or sc_in == '1':
                            print('Number of ions?')
                            nx = int(input())
                            ny = 0
                            nz = 0
                            qdoion = True
                        elif sc_in == 'dim' or sc_in == '2':
                            print('Number of unit cells along X')
                            nx = int(input())
                            print('Number of unit cells along Y')
                            ny = int(input())
                            print('Number of unit cells along Z')
                            nz = int(input())
                            qdoion = False
                        elif sc_in == 'mm' or sc_in == '3':
                            break
                        print('Output file name?')
                        foname=input()
                        make_sup_cell(nx,ny,nz,foname,qnat=qdoion)
                        sux = True
                        break
                    except ValueError:
                        print('Not a recognized option')
            elif main_inp == 'exit' or main_inp == '6':
                print('Bye for now')
                exit()
        except ValueError:
            print('Not a recognized option')

        if sux:
            while True:
                print('[1] mm   ---->   return to main menu')
                print('[2] exit ---->   exit')
                n_inp = input()
                try:
                    if n_inp == 'mm' or n_inp == '1':
                        sux = False
                        break
                    elif n_inp == 'exit' or n_inp == '2':
                        print('Bye for now')
                        exit()
                except ValueError:
                    print('Not recognized option')
