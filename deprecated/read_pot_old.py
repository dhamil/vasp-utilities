import numpy as np
import os
from itertools import product
from read_osz_out import conv_en

def wpot(oho_l,gap_l,pawcore,qone = True,q110=False,mtw=False,inv=False,twod=False,ts=False,diag=False,wens=False,spin=False,w_one_pot=False):

    rcore_max = np.max(pawcore)

    #t_l = []
    if twod:
        elev = []

    if qone:
        fl = ['LOCPOT']
    else:
        hdr = 'V_prim (A**3),CFR fraction,DOS Bandgap\n'
        if wens and not spin:
            hdr='V_prim (A**3),CFR fraction,DOS Bandgap,Energy\n'
        elif wens and spin:
            hdr='V_prim (A**3),CFR fraction up,CFR fraction down,DOS Bandgap up,DOS Bandgap up,Energy\n'
        elif spin and not wens:
            hdr='V_prim (A**3),CFR fraction up,CFR fraction down,DOS Bandgap up,DOS Bandgap up\n'
        fl = []
        tfl = []
        for fli in oho_l:
            tfl.append(fli)
        for vv in tfl:
            if os.path.isfile('locpot_'+vv) and os.path.getsize('locpot_'+vv) > 0:
                fl.append('locpot_'+vv)
            elif os.path.isfile('locpot_'+vv+'.txt') and os.path.getsize('locpot_'+vv+'.txt') > 0:
                fl.append('locpot_'+vv+'.txt')
            else:
                print(('WARNING: expected file {:} or {:} to exist. Skipping over it...').format('locpot_'+vv,'locpot_'+vv+'.txt'))
    if q110:
        ofname = 'ks_pot_110.csv'
    else:
        if twod:
            ofname = 'ks_pot_2d.csv'
        else:
            ofname = 'ks_pot.csv'
    with open(ofname,'w+') as outf:

        if not qone:
            outf.write(hdr)

        try:
            for enf, afile in enumerate(fl):
                tlist=[]
                if qone:
                    oho = oho_l['only']
                    if wens:
                        if os.path.isfile('OSZICAR') and os.path.getsize('OSZICAR') > 0:
                            osz_p = conv_en('OSZICAR')
                        else:
                            osz_p = {'E':-1e6}
                else:
                    oho = oho_l[afile[7:len(afile)]]
                    if wens:
                        tname = 'osz_'+afile[7:len(afile)]
                        if os.path.isfile(tname) and os.path.getsize(tname) > 0:
                            osz_p = conv_en(tname)
                        elif os.path.isfile(tname+'.txt') and os.path.getsize(tname+'.txt') > 0:
                            osz_p = conv_en(tname+'.txt')
                        else:
                            osz_p = {'E':-1e6}

                if ts:
                    ts_l = []
                    headr = 'E Fermi=,{:} (eV), tol 1%'.format(oho)
                if diag:
                    dg_l=[]
                a = np.zeros((3,3))
                scl = 0.0
                nion = 1
                nviol = 0
                npaw = 0
                vm = [1e20,-1e20]
                cnt = 0
                cnt2 = 0
                ispin = 0
                if spin:
                    prop_up = 0.0
                    prop_dn = 0.0
                with open(afile,'r') as inp:
                    for i,v in enumerate(inp):
                        if i == 1:
                            scl = float(v.split()[0])
                        elif 1 < i < 5:
                            a[i-2] = [scl*float(u) for u in v.split()]
                            #ad[i-2] = [float(u) for u in v.split()]
                        elif i == 6:
                            nion_l = [int(y) for y in v.split()]
                            nion = int(np.sum(nion_l))
                            bas = np.zeros((nion,3))
                            cboard = [[1.0e3,-1.0e3],[1.0e3,-1.0e3],[1.0e3,-1.0e3]]
                            ttv = np.arange(0.0,2.0,1.0)
                            #ttv = np.arange(-1.0,2.01,1.0)
                            for iavec in product(ttv,ttv,ttv): # borders of computational cell
                                iax,iay,iaz = iavec
                                tv=iax*a[0]+iay*a[1]+iaz*a[2]
                                for ind in range(3):
                                    if tv[ind] < cboard[ind][0]:
                                        cboard[ind][0]=tv[ind]
                                    elif tv[ind] > cboard[ind][1]:
                                        cboard[ind][1]=tv[ind]
                            ti_l = []
                        elif i == 7:
                            bastpe = (v.split()[0]).lower()[0]
                        elif 7 < i < 8+nion:
                            tmp = [float(u) for u in v.split()]
                            if bastpe == 'd':
                                bas[i-8] = tmp[0]*a[0] + tmp[1]*a[1] +tmp[2]*a[2]
                            elif bastpe == 'c':
                                for vb in range(3):
                                    bas[i-8,vb] = tmp[vb]
                            if twod:
                                elev.append(bas[i-8,2])
                            bas_l = []
                            ttv = np.arange(-2.0,3.0,1.0)
                            for iavec in product(ttv,ttv,ttv):
                                iax,iay,iaz = iavec
                                ttl=bas[i-8]+iax*a[0]+iay*a[1]+iaz*a[2]
                                do_app=-2
                                for ind in range(3):
                                    if cboard[ind][0]-rcore_max<=ttl[ind]<=cboard[ind][1]+rcore_max:
                                        do_app+=1 # only care aobut points that lie within some radius of the computational cell
                                        # pad the borders of the cell with the maximum core radius, as we
                                        # need to ensure all ions within and closest to computational cell are considered
                                if do_app == 1:
                                    bas_l.append(ttl)
                            ti_l.append(bas_l)
                        elif i == 9 + nion:
                            if twod:
                                elev = np.asarray(elev)
                                zmin = np.min(elev)
                                zmax = np.max(elev)
                            lns = [int(u) for u in v.split()]
                            mesh = np.zeros((lns[0]*lns[1]*lns[2],3))
                            mesr = np.zeros(lns[0]*lns[1]*lns[2],dtype='int')-nion+1
                            cnt = 0
                            #for u2 in range(lns[2]):
                            #    for u1 in range(lns[1]):
                            #        for u0 in range(lns[0]):
                            for uvec in product(np.arange(0.0,lns[2],1.0),np.arange(0.0,lns[1],1.0),np.arange(0.0,lns[0],1.0)):
                                u2,u1,u0 = uvec
                                mesh[cnt] = a[0]*u0/float(lns[0]) + a[1]*u1/float(lns[1]) + a[2]*u2/float(lns[2])
                                rcr = -1
                                kswtch = False
                                for ptpe,vcr in enumerate(nion_l):
                                    for ucr in range(vcr):
                                        rcr += 1
                                        #rad_l = []
                                        #rad = ((mesh[cnt,0] - bas[rcr,0])**2 + (mesh[cnt,1] - bas[rcr,1])**2 +(mesh[cnt,2] - bas[rcr,2])**2)**(0.5)
                                        #rad_l.append(rad)
                                        #if rad > pawcore[ptpe]:
                                        #if np.all(np.asarray(rad_l)>pawcore[ptpe]):
                                        for x in ti_l[rcr]:
                                            trad = ((mesh[cnt,0] - x[0])**2 + (mesh[cnt,1] - x[1])**2 +(mesh[cnt,2] - x[2])**2)**(0.5)
                                            if trad <= pawcore[ptpe]:
                                                kswtch=True
                                                break
                                        if kswtch:
                                            break
                                    if kswtch:
                                        break
                                if not kswtch:
                                    mesr[cnt] += nion
                                cnt += 1
                        if i > 9 + nion:

                            vs = [float(u) for u in v.split()]

                            if ispin == 1: # in spin-polarized LOCPOT,
                                # there is a multi-line header separating
                                # spin potentials
                                if int(vs[0])==lns[0] and int(vs[1])==lns[1] and int(vs[2])==lns[2]:
                                    ispin = 2
                                continue

                            for vsi in vs:

                                if twod and not ts:
                                    if mesh[cnt2,2] > zmax or mesh[cnt2,2] < zmin:
                                        cnt2 +=1
                                        continue
                                if ts:
                                    if twod:
                                        if mesh[cnt2,2] < 0.75*a[2,2]:
                                            if abs(oho-vsi)/abs(oho) < 1.0e-2:
                                                ts_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])
                                    else:
                                        if abs(oho-vsi)/abs(oho) < 1.0e-2:
                                            ts_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])
                                    cnt2 +=1
                                    continue
                                if vsi < vm[0]:
                                    vm[0] = vsi
                                elif vsi > vm[1]:
                                    vm[1] = vsi
                                if oho < vsi and mesr[cnt2] == 1:
                                    qviol = True
                                    nviol += 1
                                #elif oho < vsi and mesr[cnt2] < 1:
                                #    print('cfr in core')
                                #    t_l.append(mesh[cnt2])
                                else:
                                    qviol = False
                                if q110:
                                    if abs(mesh[cnt2,0] - mesh[cnt2,1]) < 1.0e-8 :
                                        outf.write(('{:},{:},{:}\n').format(mesh[cnt2,0],mesh[cnt2,2],oho-vsi))
                                elif diag:
                                    if abs(mesh[cnt2,2] - (mesh[cnt2,1] + mesh[cnt2,0])/2.0)< 1.0e-8 :
                                        tw = 0
                                        if qviol:
                                            tw = 1
                                        dg_l.append([mesh[cnt2,0]/scl,mesh[cnt2,1]/scl,tw])
                                        #dg_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])

                                else:
                                    if mesr[cnt2] == 1:
                                        if qone and w_one_pot:
                                            outf.write(("{:},{:},{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],vsi,qviol))
                                        elif mtw:
                                            if qviol and not inv:
                                                tlist.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],oho-vsi])
                                    if inv and not qviol and mtw:
                                        tlist.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],oho-vsi])
                                if oho > vsi and mesr[cnt2] == 1:
                                    npaw += 1
                                elif oho > vsi and mesr[cnt2] > 1:
                                    print('WARNING: check mesr')
                                cnt2 +=1
                                if spin and cnt2==lns[0]*lns[1]*lns[2] and ispin == 0:
                                    prop_up = (1.0*nviol)/(1.0*cnt2)
                                    nviol = 0
                                    cnt2 = 0
                                    ispin=1
                                elif spin and cnt2==lns[0]*lns[1]*lns[2] and ispin == 2:
                                    prop_dn = (1.0*nviol)/(1.0*cnt2)
                #np.savetxt('test.csv',np.asarray(t_l),delimiter=',')
                if diag:
                    np.savetxt('diag_'+afile[7:len(afile)]+'.csv',np.asarray(dg_l),delimiter=',',header='a1,a2,cfr')
                if ts:
                    np.savetxt(afile+'_ts.csv',np.asarray(ts_l),delimiter=',',header=headr)
                    return
                if mtw:
                    if inv:
                        np.savetxt(afile+'inv.csv',np.asarray(tlist),delimiter=',')
                    else:
                        np.savetxt(afile+'.csv',np.asarray(tlist),delimiter=',')
                if twod:
                    c_vol = np.abs(np.cross(a[0],a[1])[2])
                else:
                    c_vol = float(np.linalg.det(a))
                #print(100.0*npaw/(1.0*cnt))
                prop = nviol/(1.0*(cnt2))
                if not qone:
                    if wens and not spin:
                        ostr = [c_vol,prop,gap_l[afile[7:len(afile)]],osz_p['E']]
                    elif wens and spin:
                        ostr = [c_vol,prop_up,prop_dn,gap_l[afile[7:len(afile)]][0],gap_l[afile[7:len(afile)]][1],osz_p['E']]
                    elif spin and not wens:
                        ostr = [c_vol,prop_up,prop_dn,gap_l[afile[7:len(afile)]][0],gap_l[afile[7:len(afile)]][1]]
                    else:
                        ostr = [c_vol,prop,gap_l[afile[7:len(afile)]]]
                    outf.write(("{:},"*(len(ostr)-1)+"{:}\n").format(*ostr))
        except IOError:
            print(('LOCPOT file(s) not present: {:}').format(afile))
            if qone:
                exit()
    #pt_vol = float(np.linalg.det(np.asarray([a[0]/float(lns[0]),a[1]/float(lns[1]),a[2]/float(lns[2])])))
    #viol_vol = nviol*pt_vol
    if qone:
        if twod:
            fsr = 'ks_pot_stats_2d.txt'
        else:
            fsr = 'ks_pot_stats.txt'
        with open(fsr,'w+') as stts:
            str=('Highest occupied one-e energy {:} eV \n').format(oho)
            str+=( 'Minimum potential value {:} eV \n').format(vm[0])
            str+=( 'Maximum potential value {:} eV \n').format(vm[1])
            str+=('eps_F - max(v_s) = {:}\n').format(oho-vm[1])
            if spin:
                str+=( '{:} up-spin classically forbidden r points out of {:} total, or {:}% \n').format(prop_up*cnt2,cnt2,100.0*prop_up)
                str+=( '{:} down-spin classically forbidden r points out of {:} total, or {:}% \n').format(prop_dn*cnt2,cnt2,100.0*prop_dn)
                str+=(('{:} eV gap in up-spin DOS\n').format(gap_l['only'][0]))
                str+=(('{:} eV gap in down-spin DOS\n').format(gap_l['only'][1]))
                if twod:
                    str+=('Area of up-spin classically forbidden region {:} A**2 \n').format(c_vol*prop_up)
                    str+=('Area of down-spin classically forbidden region {:} A**2 \n').format(c_vol*prop_dn)
                else:
                    str+=('Volume of up-spin classically forbidden region {:} A**3 \n').format(c_vol*prop_up)
                    str+=('Volume of up-spin classically forbidden region {:} A**3 \n').format(c_vol*prop_dn)
            else:
                str+=( '{:} classically forbidden r points out of {:} total, or {:}% \n').format(nviol,cnt2,100.0*prop)
                str+=(('{:} eV gap in DOS\n').format(gap_l['only']))
                if twod:
                    str+=('Area of classically forbidden region {:} A**2 \n').format(c_vol*prop)
                else:
                    str+=('Volume of classically forbidden region {:} A**3 \n').format(c_vol*prop)
            stts.write(str)
    return

def w_cviol(q110=False):
    inp = np.genfromtxt('ks_pot.csv',delimiter=',',dtype=None)
    if q110:
        ofname = 'cviol_110.csv'
    else:
        ofname = 'cviol.csv'
    with open(ofname,'w+') as outf:
        for ind, ln in enumerate(inp):
            if ln[4]:
                if q110:
                    if abs(ln[0] - ln[1]) < 1.0e-8 :
                        outf.write(('{:},{:},{:}\n').format(ln[0],ln[2],ln[3]))
                else:
                    outf.write(('{:},{:},{:},{:}\n').format(ln[0],ln[1],ln[2],ln[3]))
            else:
                continue
    return

if __name__=='__main__':
    w_cviol()
