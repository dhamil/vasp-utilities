program binary_reader
  implicit none

  integer, parameter :: q = selected_real_kind(10)
  integer :: i,j,irecl,nk,nbd,ispin
  character,allocatable :: tmp
  real(q),dimension(2) :: dummy
  real(q) :: rnk,rnbd,ecut,ef
  real(q),dimension(3,3) :: lv

  ! Let's be transparent in what's going on here
  ! First step is to get the length of the WAVECAR, a binary file

  open(1, file='WAVECAR', form='unformatted', access='direct',status='unknown',recl=16)
  read(1,rec=1) dummy(1),dummy(2)
  irecl = nint(dummy(1)) ! now irecl is the record length of the WAVECAR
  close(1)
  ispin = nint(dummy(2)) ! and ispin is the spin polarization
  print*,ispin ! ispin = 1 --> spin-unpolarized
  ! ispin = 2 --> spin-polarized (anti-/ferromagnetic calculation)


  ! the next step is to read in the header,
  ! which contains the number of k-points rnk, number of bands rnbd,
  ! cutoff energy ecut, Fermi energy ef,
  ! and direct lattice vectors lv
  open(1, file='WAVECAR', form='unformatted', access='direct',status='unknown',recl=irecl)
  read(1,rec=2) rnk,rnbd,ecut,((lv(i,j),i=1,3),j=1,3),ef
  close(1)
  nk = nint(rnk) ! these are stored as quadratic precision reals
  nbd = nint(rnbd) ! so we need the nearest integer values for do loops
  print*,nk,nbd,ecut,ef
  print*,lv

end program
