import os
import numpy as np

def lc_20_res():
    fl = []
    for fli in os.listdir(os.getcwd()):
      if fli.startswith('cont-'):
          fl.append(fli)
    try:
        for afile in fl:
            a = [0.0,0.0,0.0]
            with open(afile,'r') as inp:
                for i,v in enumerate(inp):
                    if i == 1:
                        scl = float(v.split()[0])
                    elif 1 < i:
                        a = [np.abs(scl*float(u)) for u in v.split()]
                        break
            if a[0] > 0.0:
                print(afile[ 5:len(afile)],2*a[0])
            elif a[1] > 0.0:
                print(afile[5:len(afile)],2*a[1])
            elif a[2] > 0.0:
                print(afile[5:len(afile)],2*a[2])
    except IOError:
        print('Input file(s) not present')
        exit()

    return

if __name__=='__main__':
    lc_20_res()
