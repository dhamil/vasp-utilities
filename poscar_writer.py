from os import sys

def main():
    opts = ['symm','a','b','c','name']
    wd = {'name':'Unknown'}
    instr = sys.argv[1:]
    for it_str,istr in enumerate(instr):
        if istr[0] == '-':
            tv = istr[1:].split('=')
            if tv[0] in opts:
                wd[tv[0]]=tv[1]

    ostr = wd['name']+'\n'
    if wd['symm'] == 'sc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  1.0 0.0 0.0\n'+'  0.0 1.0 0.0\n'+'  0.0 0.0 1.0\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] == 'fcc' or wd['symm'] == 'ds':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  0.0 0.5 0.5\n'+'  0.5 0.0 0.5\n'+'  0.5 0.5 0.0\n'
        if wd['symm'] == 'fcc':
            ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
        elif wd['symm'] == 'ds':
            ostr += ' 2\n'+' d\n'+ '-0.125 -0.125 -0.125\n'+ ' 0.125  0.125  0.125\n'
    elif wd['symm'] == 'bcc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  -0.5  0.5  0.5\n'+'   0.5 -0.5  0.5\n'+'   0.5  0.5 -0.5\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    ofl = open('POSCAR','w+')
    ofl.write(ostr)
    ofl.close()

if __name__=="__main__":
    main()
