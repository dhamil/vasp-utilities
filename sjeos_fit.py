import numpy as np
from scipy import optimize
from read_osz_out import conv_en
import matplotlib.pyplot as plt

ev_to_gpa = 160.21766340000005

def sjeos_fit_all(osz_l,plot=False,save_plot=False,name=None,vols=None,ens=None):

    # adapted from V. N. Staroverov, G. E. Scuseria, J. Tao
    # and J. P. Perdew,
    # Tests of a ladder of density functionals for bulk solids and surfaces
    # Phys. Rev. B 69, 075102 (2004)
    # DOI: 10.1103/PhysRevB.69.075102

    if vols is not None and ens is not None:
        vol_l = vols
        en_l = ens
    else:
        dos_l = ['dos'+x[3:] for x in osz_l]

        vol_l = []
        en_l = []

        for ifl,fl in enumerate(osz_l):

            with open(dos_l[ifl],'r') as tinfl:
                for iln,ln in enumerate(tinfl):
                    ln = (ln.strip()).split()
                    if iln == 0:
                        nion = float(ln[0])
                    elif iln == 1:
                        vol_l.append(float(ln[0]))
                        break

            tpars=conv_en(fl)
            en_l.append(tpars['E'])

    tfn = lambda p, x: p[3] + p[2]*x + p[1]*x**2 + p[0]*x**3
    erfn = lambda p, x, y: tfn(p,x) - y

    p0 = [1.0,1.0,1.0,1.0]


    [aas,bs,cs,ds], scs = optimize.leastsq(erfn,p0[:],args=(1.0/np.array(vol_l)**(1.0/3.0),np.array(en_l)))

    out_pars = {}

    out_pars['V0'] = (-(bs + np.sqrt(bs**2 - 3*aas*cs))/cs)**3
    aa = aas/out_pars['V0']
    bb = bs/out_pars['V0']**(2.0/3.0)
    cc = cs/out_pars['V0']**(1.0/3.0)
    out_pars['B0'] = (18*aa + 10*bb + 4*cc)/(9*out_pars['V0'])
    out_pars['B1'] = (108*aa + 50*bb+16*cc)/(27*out_pars['B0']*out_pars['V0'])
    out_pars['E0'] = aa + bb + cc + ds
    out_pars['alpha'] = aa # need these to describe other thermodyanamic quantities
    out_pars['beta'] = bb
    out_pars['gamma'] = cc
    out_pars['omega'] = ds
    out_pars['nion'] = nion

    with open('sjeos_fit.csv','w+') as outfl:
        outfl.write('SJEOS,parameters \n')
        for par in out_pars:
            outfl.write(('{:},{:}\n').format(par,out_pars[par]))
    with open('e_v_curve.csv','w+') as outfl:
        outfl.write('Unit cell volume (Ang**3), Energy (eV)\n')
        for ien,en in enumerate(en_l):
            outfl.write(('{:},{:}\n').format(vol_l[ien],en))

    if plot:
        fit = tfn([aas,bs,cs,ds],np.array(vol_l)**(-1.0/3.0))
        fig,ax = plt.subplots(figsize=(6,6))
        plt.scatter(vol_l,en_l,c='tab:blue')
        plt.plot(vol_l,fit,c='tab:blue',linestyle='--')
        ax.tick_params(axis='both',labelsize=18)
        ax.set_xlabel('$V$ ($\mathrm{\AA{}}^3)$',fontsize=20)
        ax.set_ylabel('Energy (eV)',fontsize=20)
        plt.title('SJEOS fit',fontsize=20)
        if save_plot:
            plt.savefig(name,dpi=600,bbox_inches='tight')
        else:
            plt.show()

    return out_pars

def sjeos_pressure(v_l,pars):
    """
    calculates SJEOS pressure P = - (dE/dV)_S
    if u = V0/V, where dE/dV |_V0 = 0, then
    P = [alpha u^2 + beta u^(5/3) + gamma u^(4/3)]/V0
    """
    if hasattr(v_l,'__len__'):
        v_l = np.asarray(v_l)
    vv = pars['V0']/v_l
    pressure = (pars['alpha']*vv**2 + 2.0/3.0*pars['beta']*vv**(5.0/3.0) \
        + pars['gamma']/3.0 * vv**(4.0/3.0))/pars['V0']
    return pressure

def sjeos_enthalpy(v_l,pars):
    """
    calculates SJEOS enthalpy H = E(V) - (dE/dV)_S*V
    if u = V0/V, where dE/dV |_V0 = 0, then
    E(V) = alpha u + beta u^(2/3) + gamma u^(1/3) + omega
    H = 2 alpha u + 5/3 beta u^(2/3) + 4/3 gamma u^(1/3) + omega
    """
    if hasattr(v_l,'__len__'):
        v_l = np.asarray(v_l)
    vv = pars['V0']/v_l
    enthalpy = 2*pars['alpha']*vv + 5.0/3.0*pars['beta']*vv**(2.0/3.0) \
        + 4.0/3.0*pars['gamma']*vv**(1.0/3.0) + pars['omega']
    return enthalpy

def murnaghan_enthalpy(p_l,fu,pars):
    """
    calculates the Murnaghan EoS enthalpy using parameters
    V0: equilibrium cell volume
    E0: dE/dV | V=V0
    B0: d^2 E/d V^2 | V=V0
    B1: d B/ d P | V=V0, sometimes called B0'

    See, e.g., C. Shahi, J. Sun and J. P. Perdew,
    Phys. Rev. B 97, 094111 (2018)
    doi: 10.1103/PhysRevB.97.094111
    """
    if hasattr(p_l,'__len__'):
        p_l = np.asarray(p_l)
    enthalpy = pars['E0']/fu + pars['B0']*pars['V0']/(pars['B1'] -1.0) \
        *( (1.0 + pars['B1']/pars['B0']*p_l)**(1.0 - 1.0/pars['B1']) - 1.0)
    return enthalpy

def sjeos_transition_pressure(pars1,pars2,fu1,fu2):
    """
    calculates transition pressures using the SJEOS model
    assuming pars1 and pars2 are the fitted parameters of two different
    crystalline phases of the same solid

    NB: need to scale H by number of formula units
    Returns volume per formula unit, not unit cell volume
    V/f.u. = Cell volume/(number of chemical formula units)
    V unit cell = Cell volume/(number of ion sites)
    """
    e0_1 = pars1['E0']/fu1
    v0_1 = pars1['V0']*pars1['nion']/fu1
    e0_2 = pars2['E0']/fu2
    v0_2 = pars2['V0']*pars2['nion']/fu2
    pc = (e0_1 - e0_2)/(v0_2 - v0_1)
    print(('SJEOS transition pressure from tangent line, {:} eV/A**3 = {:}, GPa').format(pc,pc*ev_to_gpa))

    dps = ['alpha','beta','gamma','omega']
    ppoly = np.zeros(4)
    # E(V) = alpha u^3 + beta u^2 + gamma u + omega
    # with u = (1/V)^(1/3)
    for iter,par in enumerate(dps):
        ppoly[iter] = pars2[par]*pars2['V0']**((3.0-iter)/3.0)/fu2 - pars1[par]*pars1['V0']**((3.0-iter)/3.0)/fu1
    v_rt = np.zeros(0)
    for av in np.roots(ppoly):
        if av < 0.0 or abs(av.imag) > 0.0:
            continue
        else:
            v_rt = np.append(v_rt,av.real)
    vv = 1.0/v_rt**3
    print('SJEOS curve crossings:')
    print(('{:},'*len(v_rt)+' A**3 (unit cell)').format(*vv))

    def res(p):
        return murnaghan_enthalpy(p,fu2,pars2) -  murnaghan_enthalpy(p,fu1,pars1)

    p_c1 = -pars1['B0']/pars1['B1']
    p_c2 = -pars2['B0']/pars2['B1']
    p_min = max(p_c1,p_c2)

    tmp = np.arange(p_min,10.0,0.00001)
    h = res(tmp)
    min_ind = np.argmin(np.abs(h))
    ptrans = tmp[min_ind]
    print(('Murnaghan transition pressure from enthalpy (difference {:} eV), {:} eV/A**3 = {:}, GPa').format(h[min_ind],ptrans,ptrans*ev_to_gpa))
    return

if __name__=="__main__":

    sjeos_fit_all(['osz_4.84.txt','osz_5.04.txt'])
