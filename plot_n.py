import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
from itertools import product

def write_dens(optd):
    opts = {'q110':False,'spin': False,'excl_paw_core':False,'fname':'CHGCAR','pawcore': None,'int_r_vec':False}
    for opt in opts:
        if opt in optd:
            opts[opt] = optd[opt]

    rcore_max = np.max(opts['pawcore'])

    a = np.zeros((3,3))
    scl = 0.0
    nion = 1
    cnt2 = 0
    vol = 0.0

    if opts['q110']:
        fnmstr = 'density_110.csv'
        if opts['spin']:
            hdr = 'x/y, z, n up, n dn'
        else:
            hdr = 'x/y, z, n\n'
    else:
        fnmstr = 'density.csv'
        if opts['spin']:
            hdr = 'x, y, z, n up, n dn'
        else:
            hdr = 'x, y, z, n\n'

    #with open(fnmstr,'w+') as outf:
    if opts['spin']:
        n_l = []
        m_l = []
        ispin = 0
    else:
        outf = open(fnmstr,'w+')
        outf.write(hdr)

    try:
        with open(opts['fname'],'r') as inp:
            for i,v in enumerate(inp):
                if i == 1:
                    scl = float(v.split()[0])
                elif 1 < i < 5:
                    a[i-2] = [scl*float(u) for u in v.split()]
                elif i == 6:
                    nion_l = [int(y) for y in v.split()]
                    nion = int(np.sum(nion_l))
                    vol = float(np.linalg.det(a)) # should be computational cell volume
                    # not unit cell volume
                    bas = np.zeros((nion,3))
                    if opts['excl_paw_core']:
                        cboard = [[1.0e3,-1.0e3],[1.0e3,-1.0e3],[1.0e3,-1.0e3]]
                        ttv = np.arange(0.0,2.0,1.0)
                        for iavec in product(ttv,ttv,ttv): # borders of computational cell
                            iax,iay,iaz = iavec
                            tv=iax*a[0]+iay*a[1]+iaz*a[2]
                            for ind in range(3):
                                if tv[ind] < cboard[ind][0]:
                                    cboard[ind][0]=tv[ind]
                                elif tv[ind] > cboard[ind][1]:
                                    cboard[ind][1]=tv[ind]
                        ti_l = []
                elif i == 7:
                    bastpe = (v.split()[0]).lower()[0]
                elif 7 < i < 8+nion:
                    tmp = [float(u) for u in v.split()]
                    if bastpe == 'd':
                        bas[i-8] = tmp[0]*a[0] + tmp[1]*a[1] +tmp[2]*a[2]
                    elif bastpe == 'c':
                        for vb in range(3):
                            bas[i-8,vb] = tmp[vb]
                    if opts['excl_paw_core']:
                        bas_l = []
                        ttv = np.arange(-2.0,3.0,1.0)
                        for iavec in product(ttv,ttv,ttv):
                            iax,iay,iaz = iavec
                            ttl=bas[i-8]+iax*a[0]+iay*a[1]+iaz*a[2]
                            do_app=-2
                            for ind in range(3):
                                if cboard[ind][0]-rcore_max<=ttl[ind]<=cboard[ind][1]+rcore_max:
                                    do_app+=1 # only care aobut points that lie within some radius of the computational cell
                                    # pad the borders of the cell with the maximum core radius, as we
                                    # need to ensure all ions within and closest to computational cell are considered
                            if do_app == 1:
                                bas_l.append(ttl)
                        ti_l.append(bas_l)
                elif i == 9 + nion:
                    lns = [int(u) for u in v.split()]
                    if opts['int_r_vec']:
                        mesh = np.zeros((lns[0]*lns[1]*lns[2],3),dtype=int)
                    else:
                        mesh = np.zeros((lns[0]*lns[1]*lns[2],3))
                    if opts['excl_paw_core']:
                        mesr = np.zeros(lns[0]*lns[1]*lns[2],dtype='int')-nion+1
                    cnt = 0
                    for uvec in product(np.arange(0.0,lns[2],1.0),np.arange(0.0,lns[1],1.0),np.arange(0.0,lns[0],1.0)):
                        u2,u1,u0 = uvec
                        if opts['int_r_vec']:
                            mesh[cnt] = [u0,u1,u2]
                        else:
                            mesh[cnt] = a[0]*u0/float(lns[0]) + a[1]*u1/float(lns[1]) + a[2]*u2/float(lns[2])
                        if opts['excl_paw_core']:
                            rcr = -1
                            kswtch = False
                            for ptpe,vcr in enumerate(nion_l):
                                for ucr in range(vcr):
                                    rcr += 1
                                    for x in ti_l[rcr]:
                                        if opts['int_r_vec']:
                                            tmp_v = a[0]*u0/float(lns[0]) + a[1]*u1/float(lns[1]) + a[2]*u2/float(lns[2])
                                            trad = ((tmp_v[0] - x[0])**2 + (tmp_v[1] - x[1])**2 +(tmp_v[2] - x[2])**2)**(0.5)
                                        else:
                                            trad = ((mesh[cnt,0] - x[0])**2 + (mesh[cnt,1] - x[1])**2 +(mesh[cnt,2] - x[2])**2)**(0.5)
                                        if trad <= opts['pawcore'][ptpe]:
                                            kswtch=True
                                            break
                                    if kswtch:
                                        break
                                if kswtch:
                                    break
                            if not kswtch:
                                mesr[cnt] += nion
                        cnt += 1
                if i > 9 + nion:
                    if v.split()[0] == 'augmentation':
                        if opts['spin'] and ispin == 1:
                            continue
                        else:
                            break
                    else:
                        nv = [float(u)/vol for u in v.split()]
                        if opts['spin']:
                            if ispin == 1:
                                # in spin-polarized CHGCAR,
                                # there are many lines separating
                                # n(r) = n_up + n_dn and m(r) = n_up - n_dn
                                try:
                                    tmp = [int(u) for u in v.split()]
                                except ValueError:
                                    continue
                                if tmp[0]==lns[0] and tmp[1]==lns[1] and tmp[2]==lns[2]:
                                    ispin = 2
                                continue

                        for nn in nv:

                            if opts['spin']:
                                if ispin == 0:
                                    n_l.append(nn)
                                elif ispin == 2:
                                    m_l.append(nn)
                            else:
                                if opts['q110']:
                                    if opts['excl_paw_core']:
                                        if abs(mesh[cnt2,0] - mesh[cnt2,1]) < 1.0e-8 and mesr[cnt2] == 1:
                                            outf.write(("{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,2],nn))
                                    else:
                                        if abs(mesh[cnt2,0] - mesh[cnt2,1]) < 1.0e-8 :
                                            outf.write(("{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,2],nn))
                                else:
                                    if opts['excl_paw_core']:
                                        if mesr[cnt2] == 1:
                                            outf.write(("{:},{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],nn))
                                    else:
                                        outf.write(("{:},{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],nn))
                            cnt2 +=1
                            if opts['spin'] and cnt2==lns[0]*lns[1]*lns[2] and ispin == 0:
                                cnt2 = 0
                                ispin=1
                            elif opts['spin'] and cnt2==lns[0]*lns[1]*lns[2] and ispin == 2:
                                break

    except IOError:
        print(("{:} file not present").format(opts['fname']))
        exit()

    if opts['spin']:
        n_l = np.asarray(n_l)
        m_l = np.asarray(m_l)
        nup = 0.5*(n_l + m_l)
        ndn = 0.5*(n_l - m_l)
        if opts['excl_paw_core']:
            ox = mesh[:,0][mesr>0]
            oy = mesh[:,1][mesr>0]
            oz = mesh[:,2][mesr>0]
            onup = nup[mesr>0]
            ondn = ndn[mesr>0]
        else:
            ox = mesh[:,0]
            oy = mesh[:,1]
            oz = mesh[:,2]
            onup = nup
            ondn = ndn
        if opts['int_r_vec']:
            np.savetxt(fnmstr,np.transpose((ox,oy,oz,onup,ondn)),delimiter=',',header=hdr,fmt='%i,%i,%i,%.18f,%.18f')
        else:
            np.savetxt(fnmstr,np.transpose((ox,oy,oz,onup,ondn)),delimiter=',',header=hdr)
    if not opts['spin']:
        outf.close()
    return

def plot_dens(): # not ready
    dat = np.loadtxt('density_110_c.csv',delimiter=',')
    [ax,ay,den] = np.transpose(dat)
    (xx,yy) = np.meshgrid(ax,ay)
    trispace = tri.Triangulation(ax,ay)
    triterp = tri.LinearTriInterpolator(trispace,den)
    dd=triterp(xx,yy)
    fig, ax = plt.subplots()
    ax.contourf(ax,ay,dd)
    plt.show()
