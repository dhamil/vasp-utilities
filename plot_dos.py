import matplotlib.pyplot as plt
import numpy as np

def pdos(flnm,ftitle,showplot=False,ggap=False,quiet=False,spin=False,record=False):
    vct = []
    en = []
    dos = []
    intdos = []
    ef = -1e20
    enmin = 1e20
    enmax = -1e20
    qbreak = 0
    cnt = 0
    apgap = None
    ispin = 0
    if spin:
        ispin = 2
        dos_dn = []
    try:
        with open(flnm,'r') as dosin:
            for ln,ctt in enumerate(dosin):
                if cnt == 0:
                    if ln == 0:
                        tmp = [float(tt) for tt in ctt.split()]
                        nion = tmp[0]
                    elif ln == 1:
                        tmp = [float(tt) for tt in ctt.split()]
                        # unit cell volume
                        wsvol = tmp[0]#nion*tmp[0]
                    elif 1 < ln < 5:
                        continue
                    elif ln==5:
                        vct = [float(tt) for tt in ctt.split()]
                        enmax = vct[0]
                        enmin = vct[1]
                        ef = vct[3]
                        cnt += 1
                elif cnt == 1:
                    tmp = [float(tt) for tt in ctt.split()]
                    if len(tmp) == 5 and abs(tmp[0]-enmax) < 1e-8:
                        cnt += 1
                        continue
                    elif len(tmp) == 3+ispin:
                        en.append(tmp[0]-ef)
                        dos.append( tmp[1]/wsvol)
                        if spin:
                            dos_dn.append(tmp[2]/wsvol)
                        else:
                            intdos.append( tmp[2])
                elif cnt == 2:
                    break
        if record: # optional saving to HDF5 file
            # not actually saving to HDF5 file here,
            # that happens in the KS potential reading
            svfl = 'tmp_'+flnm.split('.txt')[0]+'.csv'
            if spin:
                np.savetxt(svfl,np.transpose((en,dos,dos_dn)),delimiter=',')
            else:
                np.savetxt(svfl,np.transpose((en,dos)),delimiter=',')
        if spin:
            use_ef = [False,False]
        else:
            use_ef = [False,True]
        for ien,uen in enumerate(en):
            if uen > 0.0:
                break
        if dos[ien] > 0.0:
            use_ef[0] = True
            apgap = 0.0
        if spin:
            for ien_dn,uen_dn in enumerate(en):
                if uen_dn > 0.0:
                    break
            if dos_dn[ien_dn] > 0.0:
                use_ef[1] = True
                apgap_dn = 0.0
        if ggap:
            if use_ef[0]:
                if not quiet:
                    if spin:
                        print('Gapless up-spin DOS at Fermi level')
                    else:
                        print('Gapless DOS at Fermi level')
            else:
                for jen in range(ien,len(en)-1):
                    if dos[jen] > 0.0:
                        break
                apgap = (en[jen] + en[jen-1])/2.0

            if spin:
                if use_ef[1]:
                    if not quiet:
                        print('Gapless down-spin DOS at Fermi level')
                else:
                    for jen in range(ien_dn,len(en)-1):
                        if dos_dn[jen] > 0.0:
                            break
                    apgap_dn = (en[jen] + en[jen-1])/2.0
                if not quiet:
                    if spin:
                        print(('Approximate up-spin gap from DOS {:} eV').format(apgap))
                        print(('Approximate down-spin gap from DOS {:} eV').format(apgap_dn))
                    else:
                        print(('Approximate gap from DOS {:} eV').format(apgap))
        else:
            apgap = None
            apgap_dn = None
        if showplot:
            fig,ax = plt.subplots()
            if spin:
                ax.plot(en,dos,c='tab:blue',marker='$\\uparrow$')
                ax.plot(en,dos_dn,c='tab:orange',marker='$\\downarrow$')
            else:
                ax.plot(en,dos)
            plt.title(ftitle)
            ax.set_ylabel('DOS (states/(eV$\cdot\\mathrm{\AA{}}^3$))')
            ax.set_xlabel('Energy - E_F (eV)')
            ax.set_ylim(0.0,max(dos)*1.05)
            ax.vlines(0.0,ax.get_ylim()[0],ax.get_ylim()[1],colors='gray',linestyles='dashed',label='E_fermi')
            #ax.legend()
            plt.show()
        elif flnm=="DOSCAR":
            if spin:
                np.savetxt(ftitle+'.csv',np.transpose([dos,dos_dn,en]),delimiter=',',header='DOS up, DOS dn, Energy-E_F')
            else:
                np.savetxt(ftitle+'.csv',np.transpose([dos,en]),delimiter=',',header='DOS, Energy-E_F')
        if spin:
            return ef,use_ef,[apgap,apgap_dn]
        else:
            return ef, use_ef,apgap
    except IOError:
        print('DOSCAR file not present')
        exit()
