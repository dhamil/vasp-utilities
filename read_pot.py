import numpy as np
import os
from itertools import product
from read_osz_out import conv_en
import h5py

def wpot(oho_l,gap_l,pawcore,optd):

    opts = {'qone': True,'q110':False,'mtw':False,'inv':False,'twod':False,'ts':False,'diag':False,
    'wens':False,'spin':False,'w_one_pot':False,'no_multi_write': False,'incl_paw_core':False,
    'record':False,'multi_core':False}
    for opt in opts:
        if opt in optd:
            opts[opt] = optd[opt]

    rcore_max = np.max(pawcore)

    if opts['record']: # optional saving to HDF5 record
        if opts['multi_core']:
            multi_key = list(oho_l.keys())[0]
            svfl = h5py.File("ks_pot"+multi_key+".hdf5", "w")
        else:
            svfl = h5py.File("ks_pot.hdf5", "w")

    #t_l = []
    if opts['twod']:
        elev = []

    if opts['qone']:
        fl = ['LOCPOT']
        if opts['q110']:
            ofname = 'ks_pot_110.csv'
            hdr = 'x/y, z, eps_F - v_KS\n'
        else:
            if opts['twod']:
                ofname = 'ks_pot_2d.csv'
            else:
                hdr = 'x,y,z,v_KS,CFR?\n'
                ofname = 'ks_pot.csv'
    else:
        ofname = 'ks_pot.csv'
        hdr = 'V_UC (A**3),CFR fraction,DOS Bandgap\n'
        if opts['wens'] and not opts['spin']:
            hdr='V_UC (A**3),CFR fraction,DOS Bandgap,Energy\n'
        elif opts['wens'] and opts['spin']:
            hdr='V_UC (A**3),CFR fraction up,CFR fraction down,DOS Bandgap up,DOS Bandgap up,Energy\n'
        elif opts['spin'] and not opts['wens']:
            hdr='V_UC (A**3),CFR fraction up,CFR fraction down,DOS Bandgap up,DOS Bandgap up\n'
        fl = []
        tfl = []
        for fli in oho_l:
            tfl.append(fli)
        for vv in tfl:
            if os.path.isfile('locpot_'+vv) and os.path.getsize('locpot_'+vv) > 0:
                fl.append('locpot_'+vv)
            elif os.path.isfile('locpot_'+vv+'.txt') and os.path.getsize('locpot_'+vv+'.txt') > 0:
                fl.append('locpot_'+vv+'.txt')
            else:
                print(('WARNING: expected file {:} or {:} to exist. Skipping over it...').format('locpot_'+vv,'locpot_'+vv+'.txt'))

    opened_file = False
    if opts['qone']:
        if opts['w_one_pot'] or opts['q110']:
            outf = open(ofname,'w+')
            outf.write(hdr)
            opened_file=True
    else:
        outf = open(ofname,'w+')
        outf.write(hdr)
        opened_file = True

    try:
        for enf, afile in enumerate(fl):
            tlist=[]
            if opts['qone']:
                oho = oho_l['only']
                if opts['wens']:
                    if os.path.isfile('OSZICAR') and os.path.getsize('OSZICAR') > 0:
                        osz_p = conv_en('OSZICAR')
                    else:
                        osz_p = {'E':-1e6}
            else:
                oho = oho_l[afile[7:len(afile)]]
                if opts['wens']:
                    tname = 'osz_'+afile[7:len(afile)]
                    if os.path.isfile(tname) and os.path.getsize(tname) > 0:
                        osz_p = conv_en(tname)
                    elif os.path.isfile(tname+'.txt') and os.path.getsize(tname+'.txt') > 0:
                        osz_p = conv_en(tname+'.txt')
                    else:
                        osz_p = {'E':-1e6}

            if opts['ts']:
                ts_l = []
                headr = 'E Fermi=,{:} (eV), tol 1%'.format(oho)
            if opts['diag']:
                dg_l=[]
            a = np.zeros((3,3))
            scl = 0.0
            nion = 1
            nviol = 0
            npaw = 0
            vm = [1e20,-1e20]
            cnt = 0
            cnt2 = 0
            ispin = 0
            if opts['spin']:
                prop_up = 0.0
                prop_dn = 0.0
            with open(afile,'r') as inp:
                for i,v in enumerate(inp):
                    if i == 1:
                        scl = float(v.split()[0])
                    elif 1 < i < 5:
                        a[i-2] = [scl*float(u) for u in v.split()]
                        #ad[i-2] = [float(u) for u in v.split()]
                    elif i == 6:
                        nion_l = [int(y) for y in v.split()]
                        nion = int(np.sum(nion_l))
                        bas = np.zeros((nion,3))
                        cboard = [[1.0e3,-1.0e3],[1.0e3,-1.0e3],[1.0e3,-1.0e3]]
                        ttv = np.arange(0.0,2.0,1.0)
                        #ttv = np.arange(-1.0,2.01,1.0)
                        for iavec in product(ttv,ttv,ttv): # borders of computational cell
                            iax,iay,iaz = iavec
                            tv=iax*a[0]+iay*a[1]+iaz*a[2]
                            for ind in range(3):
                                if tv[ind] < cboard[ind][0]:
                                    cboard[ind][0]=tv[ind]
                                elif tv[ind] > cboard[ind][1]:
                                    cboard[ind][1]=tv[ind]
                        ti_l = []
                    elif i == 7:
                        bastpe = (v.split()[0]).lower()[0]
                    elif 7 < i < 8+nion:
                        tmp = [float(u) for u in v.split()]
                        if bastpe == 'd':
                            bas[i-8] = tmp[0]*a[0] + tmp[1]*a[1] +tmp[2]*a[2]
                        elif bastpe == 'c':
                            for vb in range(3):
                                bas[i-8,vb] = tmp[vb]
                        if opts['twod']:
                            elev.append(bas[i-8,2])
                        bas_l = []
                        ttv = np.arange(-2.0,3.0,1.0)
                        for iavec in product(ttv,ttv,ttv):
                            iax,iay,iaz = iavec
                            ttl=bas[i-8]+iax*a[0]+iay*a[1]+iaz*a[2]
                            do_app=-2
                            for ind in range(3):
                                if cboard[ind][0]-rcore_max<=ttl[ind]<=cboard[ind][1]+rcore_max:
                                    do_app+=1 # only care aobut points that lie within some radius of the computational cell
                                    # pad the borders of the cell with the maximum core radius, as we
                                    # need to ensure all ions within and closest to computational cell are considered
                            if do_app == 1:
                                bas_l.append(ttl)
                        ti_l.append(bas_l)
                    elif i == 9 + nion:
                        if opts['twod']:
                            elev = np.asarray(elev)
                            zmin = np.min(elev)
                            zmax = np.max(elev)
                        lns = [int(u) for u in v.split()]
                        mesh = np.zeros((lns[0]*lns[1]*lns[2],3))
                        mesr = np.zeros(lns[0]*lns[1]*lns[2],dtype='int')-nion+1
                        if opts['record']: # initializing output for HDF5 record
                            if opts['spin']:
                                tout = np.zeros((lns[0]*lns[1]*lns[2],6))
                            else:
                                tout = np.zeros((lns[0]*lns[1]*lns[2],5))
                        cnt = 0
                        #for u2 in range(lns[2]):
                        #    for u1 in range(lns[1]):
                        #        for u0 in range(lns[0]):
                        for uvec in product(np.arange(0.0,lns[2],1.0),np.arange(0.0,lns[1],1.0),np.arange(0.0,lns[0],1.0)):
                            u2,u1,u0 = uvec
                            mesh[cnt] = a[0]*u0/float(lns[0]) + a[1]*u1/float(lns[1]) + a[2]*u2/float(lns[2])
                            rcr = -1
                            kswtch = False
                            for ptpe,vcr in enumerate(nion_l):
                                for ucr in range(vcr):
                                    rcr += 1
                                    #rad_l = []
                                    #rad = ((mesh[cnt,0] - bas[rcr,0])**2 + (mesh[cnt,1] - bas[rcr,1])**2 +(mesh[cnt,2] - bas[rcr,2])**2)**(0.5)
                                    #rad_l.append(rad)
                                    #if rad > pawcore[ptpe]:
                                    #if np.all(np.asarray(rad_l)>pawcore[ptpe]):
                                    for x in ti_l[rcr]:
                                        trad = ((mesh[cnt,0] - x[0])**2 + (mesh[cnt,1] - x[1])**2 +(mesh[cnt,2] - x[2])**2)**(0.5)
                                        if trad <= pawcore[ptpe]:
                                            kswtch=True
                                            break
                                    if kswtch:
                                        break
                                if kswtch:
                                    break
                            if not kswtch:
                                mesr[cnt] += nion
                            cnt += 1
                    if i > 9 + nion:

                        vs = [float(u) for u in v.split()]

                        if ispin == 1: # in spin-polarized LOCPOT,
                            # there is a multi-line header separating
                            # spin potentials
                            if int(vs[0])==lns[0] and int(vs[1])==lns[1] and int(vs[2])==lns[2]:
                                ispin = 2
                            continue

                        for vsi in vs:

                            if opts['record']:
                                if opts['spin']:
                                    if ispin == 0: # only need to write grid and Boolean in PAW core
                                        tout[cnt2,:3] = mesh[cnt2] # once, do this on first pass
                                        if mesr[cnt2] == 1:
                                            tout[cnt2,-1] = 0.0
                                        else:
                                            tout[cnt2,-1] = 1.0
                                        tout[cnt2,3] = vsi
                                    elif ispin == 2:
                                        tout[cnt2,4] = vsi
                                else: # storing outputs to write to hdf5 file
                                    tout[cnt2,:3] = mesh[cnt2]
                                    if mesr[cnt2] == 1:
                                        tout[cnt2,-1] = 0.0
                                    else:
                                        tout[cnt2,-1] = 1.0
                                    tout[cnt2,3] = vsi

                            if opts['twod'] and not opts['ts']:
                                if mesh[cnt2,2] > zmax or mesh[cnt2,2] < zmin:
                                    cnt2 +=1
                                    continue
                            if opts['ts']:
                                if opts['twod']:
                                    if mesh[cnt2,2] < 0.75*a[2,2]:
                                        if abs(oho-vsi)/abs(oho) < 1.0e-2:
                                            ts_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])
                                else:
                                    if abs(oho-vsi)/abs(oho) < 1.0e-2:
                                        ts_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])
                                cnt2 +=1
                                continue
                            if vsi < vm[0]:
                                vm[0] = vsi
                            elif vsi > vm[1]:
                                vm[1] = vsi
                            if oho < vsi and mesr[cnt2] == 1:
                                qviol = True
                                nviol += 1
                            #elif oho < vsi and mesr[cnt2] < 1:
                            #    print('cfr in core')
                            #    t_l.append(mesh[cnt2])
                            else:
                                qviol = False
                            if opts['q110']:
                                if abs(mesh[cnt2,0] - mesh[cnt2,1]) < 1.0e-8 :
                                    outf.write(('{:},{:},{:}\n').format(mesh[cnt2,0],mesh[cnt2,2],oho-vsi))
                            elif opts['diag']:
                                if abs(mesh[cnt2,2] - (mesh[cnt2,1] + mesh[cnt2,0])/2.0)< 1.0e-8 :
                                    tw = 0
                                    if qviol:
                                        tw = 1
                                    dg_l.append([mesh[cnt2,0]/scl,mesh[cnt2,1]/scl,tw])
                                    #dg_l.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2]])

                            else:
                                if opts['incl_paw_core']:
                                    if opts['qone'] and opts['w_one_pot']:
                                        outf.write(("{:},{:},{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],vsi,qviol))
                                else:
                                    if mesr[cnt2] == 1:
                                        if opts['qone'] and opts['w_one_pot']:
                                            outf.write(("{:},{:},{:},{:},{:}\n").format(mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],vsi,qviol))
                                        elif opts['mtw']:
                                            if qviol and not opts['inv']:
                                                tlist.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],oho-vsi])
                                if opts['inv'] and not qviol and opts['mtw']:
                                    tlist.append([mesh[cnt2,0],mesh[cnt2,1],mesh[cnt2,2],oho-vsi])
                            if oho > vsi and mesr[cnt2] == 1:
                                npaw += 1
                            elif oho > vsi and mesr[cnt2] > 1:
                                print('WARNING: check mesr')
                            cnt2 +=1
                            if opts['spin'] and cnt2==lns[0]*lns[1]*lns[2] and ispin == 0:
                                prop_up = (1.0*nviol)/(1.0*cnt2)
                                nviol = 0
                                cnt2 = 0
                                ispin=1
                            elif opts['spin'] and cnt2==lns[0]*lns[1]*lns[2] and ispin == 2:
                                prop_dn = (1.0*nviol)/(1.0*cnt2)
            #np.savetxt('test.csv',np.asarray(t_l),delimiter=',')
            if opts['diag']:
                np.savetxt('diag_'+afile[7:len(afile)]+'.csv',np.asarray(dg_l),delimiter=',',header='a1,a2,cfr')
            if opts['ts']:
                np.savetxt(afile+'_ts.csv',np.asarray(ts_l),delimiter=',',header=headr)
                return
            if opts['mtw']:
                if opts['inv']:
                    np.savetxt(afile+'inv.csv',np.asarray(tlist),delimiter=',')
                else:
                    np.savetxt(afile+'.csv',np.asarray(tlist),delimiter=',')
            if opts['twod']: # unit cell volumes
                c_vol = np.abs(np.cross(a[0],a[1])[2])/(1.0*nion)
            else:
                c_vol = float(np.linalg.det(a))/(1.0*nion)
            #print(100.0*npaw/(1.0*cnt))
            prop = nviol/(1.0*(cnt2))
            if not opts['qone']:
                if opts['wens'] and not opts['spin']:
                    ostr = [c_vol,prop,gap_l[afile[7:len(afile)]],osz_p['E']]
                elif opts['wens'] and opts['spin']:
                    ostr = [c_vol,prop_up,prop_dn,gap_l[afile[7:len(afile)]][0],gap_l[afile[7:len(afile)]][1],osz_p['E']]
                elif opts['spin'] and not opts['wens']:
                    ostr = [c_vol,prop_up,prop_dn,gap_l[afile[7:len(afile)]][0],gap_l[afile[7:len(afile)]][1]]
                else:
                    ostr = [c_vol,prop,gap_l[afile[7:len(afile)]]]
                if opts['no_multi_write']:
                    if not opts['record']:
                        return ostr
                else:
                    outf.write(("{:},"*(len(ostr)-1)+"{:}\n").format(*ostr))

            if opts['record']:
                tgrp = svfl.create_group(str(c_vol))
                ks_pot_set=tgrp.create_dataset('KS potential',data=tout) # HDF5 group, keys are volumes/atom
                lv = tgrp.create_dataset('lattice vectors',data=a)
                lv.attrs['header']= 'x,y,z'
                bv=tgrp.create_dataset('basis vectors',data=bas)
                bv.attrs['header']= 'x,y,z'
                tgrp.create_dataset('ions',data=nion_l)
                tgrp.create_dataset('paw core radii',data=pawcore)
                if opts['qone']:
                    dos_flnm = 'tmp_DOSCAR.csv'
                else:
                    dos_flnm = 'tmp_dos_'+afile[7:len(afile)].split('.txt')[0]+'.csv'
                dos_dat = np.genfromtxt(dos_flnm,delimiter=',')
                dos_set=tgrp.create_dataset('DOS',data=dos_dat)
                if opts['spin']:
                    dos_set.attrs['header']='E - E_fermi, DOS up 1/(eV*angstrom**3), DOS down'
                else:
                    dos_set.attrs['header']='E - E_fermi, DOS 1/(eV*angstrom**3)'
                os.system(('rm -f {:}').format(dos_flnm))
                tgrp.attrs['V_uc']=c_vol # include meta-data relevant for paper
                tgrp.attrs['E_fermi']=oho # these are either extracted or analyzed from VASP outputs
                if opts['qone']:
                    atrr_key = 'only'
                else:
                    atrr_key = afile[7:len(afile)]
                if opts['wens']:
                    tgrp.attrs['E conv']=osz_p['E']
                if opts['spin']:
                    tgrp.attrs['gap up']=gap_l[atrr_key][0]
                    tgrp.attrs['cfr frac up']=prop_up
                    tgrp.attrs['gap down']=gap_l[atrr_key][1]
                    tgrp.attrs['cfr frac down']=prop_dn
                    ks_pot_set.attrs['header']='x, y, z, KS potential up, KS potential down, PAW Core? (0=no, 1=yes)'
                else:
                    tgrp.attrs['gap']=gap_l[atrr_key]
                    tgrp.attrs['cfr frac']=prop
                    ks_pot_set.attrs['header']='x, y, z, KS potential, PAW Core? (0=no, 1=yes)'
                    tgrp.attrs['units']='energies in eV, distances in angstrom'
                svfl.close()
                if opts['no_multi_write']:
                    return ostr

    except IOError:
        print(('LOCPOT file(s) not present: {:}').format(afile))
        if opts['qone']:
            exit()
    if opened_file:
        outf.close()
    #pt_vol = float(np.linalg.det(np.asarray([a[0]/float(lns[0]),a[1]/float(lns[1]),a[2]/float(lns[2])])))
    #viol_vol = nviol*pt_vol
    if opts['qone']:
        if opts['twod']:
            fsr = 'ks_pot_stats_2d.txt'
        else:
            fsr = 'ks_pot_stats.txt'
        with open(fsr,'w+') as stts:
            lstr=('Highest occupied one-e energy {:} eV \n').format(oho)
            lstr+=( 'Minimum potential value {:} eV \n').format(vm[0])
            lstr+=( 'Maximum potential value {:} eV \n').format(vm[1])
            lstr+=('eps_F - max(v_s) = {:}\n').format(oho-vm[1])
            if opts['spin']:
                lstr+=( '{:} up-spin classically forbidden r points out of {:} total, or {:}% \n').format(prop_up*cnt2,cnt2,100.0*prop_up)
                lstr+=( '{:} down-spin classically forbidden r points out of {:} total, or {:}% \n').format(prop_dn*cnt2,cnt2,100.0*prop_dn)
                lstr+=(('{:} eV gap in up-spin DOS\n').format(gap_l['only'][0]))
                lstr+=(('{:} eV gap in down-spin DOS\n').format(gap_l['only'][1]))
                if opts['twod']:
                    lstr+=('Area of up-spin classically forbidden region {:} A**2 \n').format(c_vol*prop_up)
                    lstr+=('Area of down-spin classically forbidden region {:} A**2 \n').format(c_vol*prop_dn)
                else:
                    lstr+=('Volume of up-spin classically forbidden region {:} A**3 \n').format(c_vol*prop_up)
                    lstr+=('Volume of up-spin classically forbidden region {:} A**3 \n').format(c_vol*prop_dn)
            else:
                lstr+=( '{:} classically forbidden r points out of {:} total, or {:}% \n').format(nviol,cnt2,100.0*prop)
                lstr+=(('{:} eV gap in DOS\n').format(gap_l['only']))
                if opts['twod']:
                    lstr+=('Area of classically forbidden region {:} A**2 \n').format(c_vol*prop)
                else:
                    lstr+=('Volume of classically forbidden region {:} A**3 \n').format(c_vol*prop)
            stts.write(lstr)
    return

def w_cviol(q110=False):
    inp = np.genfromtxt('ks_pot.csv',delimiter=',',dtype=None)
    if q110:
        ofname = 'cviol_110.csv'
    else:
        ofname = 'cviol.csv'
    with open(ofname,'w+') as outf:
        for ind, ln in enumerate(inp):
            if ln[4]:
                if q110:
                    if abs(ln[0] - ln[1]) < 1.0e-8 :
                        outf.write(('{:},{:},{:}\n').format(ln[0],ln[2],ln[3]))
                else:
                    outf.write(('{:},{:},{:},{:}\n').format(ln[0],ln[1],ln[2],ln[3]))
            else:
                continue
    return

if __name__=='__main__':
    w_cviol()
