import ase.io.vasp
import ase.build
import plot_n
import read_pot
import gap
import plot_dos
import os
import time
import numpy as np
import multiprocessing as mp
import h5py
from sjeos_fit import sjeos_fit_all,sjeos_transition_pressure
from inter_main import inter_main
import reap_sow
#from os import getcwd,sys,system,remove


# Utilities for VASP
# written by Aaron Kaplan
# last modification, 4 December, 2020

def make_sup_cell(nx,ny,nz,fname,qnat=True):
    try:
        cell = ase.io.vasp.read_vasp("CONTCAR")
        if qnat:
            tmx = ase.build.find_optimal_cell_shape(cell.cell,nx,'sc')
            spcell = ase.build.make_supercell(cell,tmx)
        else:
            spcell=cell*(nx,ny,nz)
        ase.io.vasp.write_vasp(fname,spcell, label='spcell',direct=False,sort=True)
        return
    except IOError:
        print("CONTCAR unavailable")
        return

def get_paw_core():
    a0 = 0.529177210903 # Bohr radius from NIST, needed for PAW core radius
    # from VASP, this is given as 0.529177249 (found this much later)
    # shouldn't make a substantial difference, as RCORE values are generally
    # 1 - 2 bohr, so the difference is ~ 1 x 10**(-7) angstrom
    # gave no difference in Ne, which seemed to be most sensitive
    if os.path.isfile('OUTCAR') and os.path.getsize('OUTCAR')>0:
        os.system("grep 'RCORE' OUTCAR > tmpflks.txt")
    elif os.path.isfile('POTCAR') and os.path.getsize('POTCAR')>0:
        os.system("grep 'RCORE' POTCAR > tmpflks.txt")
    else:
        print('Error: need OUTCAR or POTCAR to get PAW core radius.')
        raise IOError
    l = []
    with open('tmpflks.txt','r') as tmpfl:
        for u in tmpfl:
            l.append(u.split())
    os.remove('tmpflks.txt')

    return [float(l[i][2])*a0 for i in range(len(l))]

if __name__=='__main__':

    gtho = False
    sux = False

    #cstr = [None for i in range(7)]
    #for i,u in enumerate(os.sys.argv):
    #    if u is not '.':
    #        cstr[i] = u
    main_l = ['-gap','-pot','-dos','-spc','-dens','-sjeos','-inter','-reap','-sow','-help','-h']
    opts = {'-gap': ['-name','-dosgap'],
    '-pot': ['-full','-multi','-viol','-ens','-110','-diag','-2d','-2D','-ts','-mw','-car','-name','-inv','-spin','-ncore','-wone','-incl_paw_core','-hdf5'],
    '-dens' : ['-full','-110','-spin','-nocore','-name','-int_latt'],
    '-dos' : ['-name','-show','-title','-gap','-spin'],
    '-spc' : ['-name','-show','-ion','-dim'],
    '-sjeos': ['-plot','-save','-name','-files','-ptrans','-ftype','-fu'],
    '-sow': ['-np','-files'],
    '-reap': ['-dirs'],
    '-h': main_l
    }
    cstr = {}
    instr = os.sys.argv[1:]
    for it_str,istr in enumerate(instr):
        if istr[0] == '-':
            if istr in main_l:
                cstr['main'] = istr
                break
        else:
            raise SystemExit('Arguments must be prefaced by -')

    regex = ['-name', '-title', '-dim', '-ion', '-files', '-dirs', '-fu']

    if cstr['main'] in opts:
        for anopt in opts[cstr['main']]:
            if anopt in regex:
                def_val = None
            elif anopt == '-ncore':
                def_val = 1
            elif anopt == '-ftype':
                def_val = 'ev'
            else:
                def_val = False
            cstr[anopt] = def_val
        for it_str,istr in enumerate(instr):
            if istr[0] == '-':
                if istr in opts[cstr['main']]:
                    cstr[istr] = True
            else:
                if instr[it_str-1] == '-ncore':
                    istr = int(istr)
                cstr[instr[it_str-1]] = istr
    else:
        raise SystemExit('Not a recognized program')

    if cstr['main'] == "-gap":
        fname = 'EIGENVAL'
        if cstr['-name'] is not None:
            fname = cstr['-name']
        if cstr['-dosgap']:
            fname = 'DOSCAR'
            _,_,_=plot_dos.pdos(fname,'dos',ggap=True)
        else:
            gap.gap(fname)
    elif cstr['main'] == "-pot":
        do2d = False
        if cstr['-2d'] or cstr['-2D']:
            do2d = True
        fname = 'EIGENVAL'
        if cstr['-name'] is not None:
            fname = cstr['-name']
        optd = {'q110': cstr['-110'],'mtw': cstr['-mw'],'inv': cstr['-inv'],'twod':do2d,'ts':cstr['-ts'],
        'diag': cstr['-diag'],'wens': cstr['-ens'],'spin': cstr['-spin'],'w_one_pot': cstr['-wone'],
        'incl_paw_core':cstr['-incl_paw_core'],'record':cstr['-hdf5']}
        if cstr['-ncore']> 1:
            optd['multi_core']=True
        if cstr['-full']:
            optd['qone'] = True
            ef,met,gap = plot_dos.pdos('DOSCAR','dos',ggap=True,spin=optd['spin'],record=cstr['-hdf5'])
            epsho = {'only' : ef}
            gapl = {'only' : gap}
            #if met:
            #    gapl = {'only' : 0.0}
            #else:
            #    _,gapl=gap.gap(fname,qone =True)
            pcr = get_paw_core()
            read_pot.wpot(epsho,gapl,pcr,optd)
        elif cstr['-multi']:
            optd['qone'] = False
            epsho = {}
            gapl = {}
            tf_l = []
            tf_l2 = []
            for fli in os.listdir(os.getcwd()):
                if fli.startswith('dos_'):
                    tf_l.append(fli)
                    ttf = fli.split('_')[1]
                    tf_l2.append(float(ttf.split('.txt')[0]))
            tf_l = np.asarray(tf_l)
            tf_l = tf_l[np.argsort(tf_l2)]
            for fli in tf_l:
                ef,met,tgap = plot_dos.pdos(fli,fli,ggap=True,quiet=True,spin=optd['spin'],record=cstr['-hdf5'])
                epsho[fli[4:len(fli)]] = ef
                gapl[fli[4:len(fli)]] = tgap
                """
                if met:
                    gapl[fli[4:len(fli)]] = 0.0
                else:
                    tho,tgap=gap.gap('eig_'+fli[4:len(fli)],qone=True,quiet=True)
                    gapl[fli[4:len(fli)]] = tgap['only']
                """
            pcr = get_paw_core()
            if cstr['-ncore'] > 1: # multi-core processing
                optd['no_multi_write']=True
                pool = mp.Pool(processes=min(cstr['-ncore'],len(tf_l)))
                ibl = []
                for fli in epsho:
                    ibl.append(({fli: epsho[fli]},{fli:gapl[fli]},pcr,optd))
                t_out = pool.starmap(read_pot.wpot,ibl)
                pool.close()
                with open('ks_pot.csv','a') as ofl: # first compile analyzed data into
                    for astr in t_out: # single output
                        ofl.write(("{:},"*(len(astr)-1)+"{:}\n").format(*astr))
                if cstr['-hdf5']: # if recording to HDF5 file, compile individual data files
                    toutfl = h5py.File("ks_pot.hdf5", "w") # into one single output file here
                    for fli in epsho:
                        tinfl = h5py.File("ks_pot"+fli+".hdf5", "r")
                        the_key = list(tinfl.keys())[0]
                        tgrp = toutfl.create_group(the_key)
                        for anat in tinfl[the_key].attrs:
                            tgrp.attrs[anat] = tinfl[the_key].attrs[anat]
                        for akey in list(tinfl[the_key].keys()):
                            tset=tgrp.create_dataset(akey,data=tinfl[the_key][akey])
                            for anat in tinfl[the_key][akey].attrs:
                                tset.attrs[anat]=tinfl[the_key][akey].attrs[anat]
                        tinfl.close()
                        os.system(('rm -f {:}').format("ks_pot"+fli+".hdf5"))
                    toutfl.close()

            else:
                read_pot.wpot(epsho,gapl,pcr,optd)
        elif cstr['-viol']:
            if os.path.isfile('ks_pot.csv') and os.path.getsize('ks_pot.csv') > 0:
                read_pot.w_cviol(q110=d110)
            else:
                optd['qone'] = True
                #epsho,gapl=gap.gap(fname,qone =True)
                ef,met,gap = plot_dos.pdos('DOSCAR','dos',spin=optd['spin'])
                epsho = {'only' : ef}
                gapl = {'only' : gap}
                pcr = get_paw_core()
                read_pot.wpot(epsho,gapl,pcr,optd)
                read_pot.w_cviol(q110=d110)
        else:
            raise ValueError('Not a recognized second flag')
    elif cstr['main'] == "-dens":
        optd = {'q110':cstr['-110'],'spin': cstr['-spin'],'excl_paw_core':cstr['-nocore'],'int_r_vec':cstr['-int_latt']}
        if cstr['-full']:
            optd['q110']=False
        elif cstr['-110']:
            optd['q110']=True
        if cstr['-name'] is not None:
            optd['fname']=cstr['-name']
        optd['pawcore'] = None
        if cstr['-nocore']:
            optd['pawcore'] = get_paw_core()
        plot_n.write_dens(optd)
    elif cstr['main'] == "-dos":
        fln = 'DOSCAR'
        if cstr['-name'] is not None:
            fln = cstr['-name']
        ptitle = 'DOS'
        if cstr['-title'] is not None:
            ptitle = cstr['-title']
        _,_,_=plot_dos.pdos(fln,ptitle,showplot=cstr['-show'],ggap=cstr['-gap'],spin=cstr['-spin'])
    elif cstr['main'] == "-spc":
        if cstr['-ion'] is not None:
            nx = int(float(cstr['-ion']))
            ny = 0
            nz = 0
            qdoion = True
            foname = cstr['-name']
        elif cstr['-dim'] is not None:
            dims = [float( x) for x in cstr['-dim'].split(',')]
            nx = int(dims[0])
            ny = int(dims[1])
            nz = int(dims[2])
            qdoion = False
            foname = cstr['-name']
        else:
            raise ValueError('Not a recognized second flag')
        make_sup_cell(nx,ny,nz,foname,qnat=qdoion)
    elif cstr['main'] == '-sjeos':
        if cstr['-ptrans']:
            sjeos_regex = ['alpha','beta','gamma','omega','V0','B0','B1','E0','nion']
            wfl = cstr['-files'].split(',')
            if len(wfl) < 2:
                raise SystemExit('Must have at least two files to calculate transition pressure!')
            par1 = {}
            par2 = {}
            fu1 = 1.0
            fu2 = 1.0
            if cstr['-fu'] is not None:
                fus = cstr['-fu'].split(',')
                fu1 = float(fus[0])
                fu2 = float(fus[1])
            for ifl,fl in enumerate(wfl):
                with open(fl,'r') as tmp_infl:
                    for iln,ln in enumerate(tmp_infl):
                        if iln == 0:
                            continue
                        ln = (ln.strip()).split(',')
                        if ln[0] not in sjeos_regex:
                            raise SystemExit('For transition pressure calculation, input files must contain SJEOS parameters!')
                        if ifl == 0:
                            par1[ln[0]] = float(ln[1])
                        elif ifl == 1:
                            par2[ln[0]] = float(ln[1])
            sjeos_transition_pressure(par1,par2,fu1,fu2)
            exit()
            v1,p1,v2,p2 = sjeos_transition_pressure(par1,par2,fu1,fu2)
            with open('sjeos_trans_p.csv','w+') as ofl:
                ofl.write('Phase , 1    , | , Phase, 2    \n')
                ofl.write('Volume (A**3), Pressure (eV/A**3), | ,Volume (A**3), Pressure (eV/A**3) \n')
                for iv in range(len(v1)):
                    ofl.write(('{:},{:}, | ,{:},{:}\n').format(v1[iv],p1[iv],v2[iv],p2[iv]))
        else:
            tnm = 'SJEOS fit'
            if cstr['-name'] is not None:
                if cstr['-name'][-4:] !='.pdf':
                    cstr['-name'] = cstr['-name'] + '.pdf'
                tnm = cstr['-name']

            if cstr['-files'] is not None:
                dat = np.genfromtxt(cstr['-files'],delimiter=',',skip_header=1)
                #if cstr['-ftype'] == 'ev': # simple, two-column E(V) data
                v = dat[:,0]
                en = dat[:,1]
                sjeos_fit_all(None,plot=cstr['-plot'],save_plot=cstr['-save'],name=tnm,vols=v,ens=en)
            else:
                tf_l = []
                tf_l2 = []
                for fli in os.listdir(os.getcwd()):
                    if fli.startswith('osz_'):
                        tf_l.append(fli)
                        ttf = fli.split('_')[1]
                        tf_l2.append(float(ttf.split('.txt')[0]))
                tf_l = np.asarray(tf_l)
                tf_l = tf_l[np.argsort(tf_l2)]
                sjeos_fit_all(tf_l,plot=cstr['-plot'],save_plot=cstr['-save'],name=tnm)
    elif cstr['main'] == '-sow':
        npc = 4
        if cstr['-np'] is not None:
            cstr['-np'] = npc
        if cstr['-files'] is not None:
            fls=cstr['-files']
        else:
            fls = []
            tf_l2 = []
            for fli in os.listdir(os.getcwd()):
                if fli.startswith('locpot_'):
                    fls.append(fli)
                    ttf = fli.split('_')[1]
                    tf_l2.append(float(ttf.split('.txt')[0]))
            fls = np.asarray(fls)
            fls = fls[np.argsort(tf_l2)]
        dirs = reap_sow.sow(npc,fls)
    elif cstr['main'] == '-reap':
        if cstr['-dirs'] is not None:
            dirs = cstr['-dirs']
        else:
            dirs = []
            for root,d_l,f_l in os.walk(os.getcwd()):
                for d in d_l:
                    if d.startswith('tmp_'):
                        dirs.append(d)
            reap_sow.reap(dirs)
    elif cstr['main'] == "-inter":
        inter_main()
    elif cstr['main'] == '-help':
        print('Command form: vuk -1 -2...')
        print('Flags can be in any order')
        print('Main program options: gap, pot, dens, dos, spc, SJEOS fitting,')
        print('sow, reap')
        print('All other options are modifiers (access subfunctions, etc.)')
        print('For more detailed information, call vuk -h')
    elif cstr['main'] == '-h':

        kindly_print=True
        for prog in main_l:
            if cstr[prog] and prog!='-h':
                print(('Options for {:} program').format(prog[1:]))
                print(opts[prog])
                kindly_print=False
        if kindly_print:
            print('Command form: vuk -1 -2...')
            print('Options for main function:')
            print('gap   ---->   Find gap from EIGENVAL')
            print('pot   ---->   Analyze KS potential from LOCPOT')
            print('dens  ---->   Write density to file from CHG')
            print('dos   ---->   Analyze density of states from DOSCAR')
            print('spc   ---->   Make a supercell from CONTCAR')
            print('sjeos ---->   Fit E(V) data to the stabilized jellium equation of state')
            print('sow   ---->   Separate multiple LOCPOT files into subdirectories')
            print('reap  ---->   Collect data from analyzed sowed directories')
            print('Call vuk -h -X to access all available flags for main function X')
    else:
        raise SystemExit('Not a recognized first flag')
