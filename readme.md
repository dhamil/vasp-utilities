***
# VASP utilities
***

## maintainer:
Aaron Kaplan
## Co-authors:
Stewart Clark, Kieron Burke, John Perdew
### Publication:
A.D. Kaplan, S.J. Clark, K. Burke, and J.P. Perdew, npj Computational Materials **7**, 25 (2021), https://doi.org/10.1038/s41524-020-00479-0
### Data repository:
A.D. Kaplan, S.J. Clark, K. Burke, and J.P. Perdew, *Calculation and interpretation of classical turning surfaces in solids*, Materials Cloud Archive 2020.169 (2020), https://doi.org/10.24435/materialscloud:2h-zq. 
***

### Primary executable: main.py

  Controls which routines are used, call
  python3 main.py -[options]

  To see a list of options, call
    python3 main.py -h, to list all major functions
    python3 main.py -h -fun, to list the flags/attributes of function fun
    python3 main.py -help, short overview of the included functions

***
### Primary routines:

  -pot:
    Obtains KS potential, bandgap, and volume of classically forbidden region
    from LOCPOT, or other files

    NB: also needs associated DOSCAR files for Fermi energy and gap,
    POTCAR or OUTCAR files for PAW core radius,
    and OSZICAR if the converged energy is desired

    options:
      -full, for analyzing only one file (e.g., equilibrium data)
      -multi, for analyzing multiple files named like:
        locpot_<ind> or locpot_<ind>.txt

    Important modifiers (others included for testing purposes, or are deprecated):
      -spin, do spin-polarized analysis
      -ens, return converged energy
      -wone, write KS potential for only one file (not written by default to save space)
      -hdf5, record data as HDF5 file
        if -full, only one group written to file
        if -multi, each group contains one geometry, tagged by cell volume
      -ncore <N>, use <N> cores to process data, one file per core
      -110, only analyze conventional cubic cell [110] direction

  -dos:
    obtains density of states data from DOSCAR, can also plot DOS data

    options:
      -name <name>, use a file called <name> instead of DOSCAR
      -show, to show the plot
      -title, to name the plot

  -sjeos:
    Fits E(V) data to the stabilized jellium equation of state
      files should be of the form osz_<ind> or osz_<ind>.txt if they are not in
      a two-column E(V) array

    options:
      -plot, plot E(V) data
      -save, save said plot
      -name <name>, name of the plot
      -ptrans, calculate transition pressure between two phases
      -files file1,file2  required option for -ptrans, files to be analyzed
      -fu <# formula units 1>,<# formula units 2> corresponding # of formula units for the files

  -dens:
    analyze density from CHGCAR, or AECCAR files

    important options:
      -full, only one file
      -110, only analyze conventional cubic [110] direction
      -spin, spin-polarized density

  -gap:

    calculates band gap, and whether it is direct or indirect from EIGENVAL
    options:
      -name <name>, use a file called <name> instead of EIGENVAL

***

### List of flags used to analyze/record the KS potential data

  Equilibrium data:
    spin-unpolarized: main.py -pot -full -hdf5
    spin-polarized: main.py -pot -full -hdf5 -spin

  Strain data:
    spin-unpolarized: main.py -pot -multi -ncore 4 -hdf5
    spin-polarized: main.py -pot -multi -ncore 4 -hdf5 -spin -ens
    crystalline phase transitions: main.py -pot -multi -ncore 4 -hdf5 -ens

  The -ncore N flag uses N cores to divide up the list of files to analyze;
  it can be omitted

***
