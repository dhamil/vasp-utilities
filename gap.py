import numpy as np
import os

def gap(rfname,qone = True,quiet=False):
    homos = {}
    gaps = {}
    if qone:
        infile = [rfname]
    else:
        infile = []
        for fli in os.listdir(os.getcwd()):
          if fli.startswith('eig_'):
              infile.append(fli)

    for afile in infile:

        try:
            with open(afile,'r') as inp:
                for i,v in enumerate(inp):
                    if i > 7:
                        if len(v.split()) == 3:
                            ox = np.zeros(3)
                            break
                        elif len(v.split()) == 5:
                            ox = np.zeros(5)
                            break
        except IOError:
            print( 'EIGENVAL file(s) not present')
            exit()

        lo = 1e20
        olu = 1e20
        oho = -1e20
        kho = np.zeros(4)
        klu = np.zeros(4)
        pwarn = 0

        with open(afile,'r') as ifl:
            for line,u in enumerate(ifl):
                lnu = len(u.split())
                if line < 7:
                    continue
                if lnu == 4:
                    kt = [float(y) for y in u.split()]
                elif lnu == 0:
                    continue
                elif lnu == 3:
                    x = [float(y) for y in u.split()]
                    #if round(x[2]) == 0 and round(ox[2]) == 1:
                    if abs(x[2]) < 1.0e-8 and ox[2]>0.0:
                        if 0.0 < abs(x[2]) < 1.0:
                            pwarn += 1
                        if x[1] < olu:
                            olu = x[1]
                            klu = [t for t in kt]
                        if ox[1] > oho:
                            oho = ox[1]
                            kho = [t for t in kt]
                        if kt[0]**2 + kt[1]**2 + kt[2]**2 < 1.0e-8:
                            glu = x[1]
                            gho = ox[1]
                    else:
                        if x[1] < lo:
                            lo = x[1]
                    ox = x
                elif lnu == 5:
                    x = [float(y) for y in u.split()]
                    #if round(x[3]) == 0 and round(ox[3]) == 1:
                    if abs(x[3]) < 1.0e-8 and ox[3]>0.0:
                        if 0.0 < abs(x[3]) < 1.0:
                            pwarn += 1
                        if x[1] < olu:
                            olu = x[1]
                            klu = [t for t in kt]
                        if ox[1] > oho:
                            oho = ox[1]
                            kho = [t for t in kt]
                        if kt[0]**2 + kt[1]**2 + kt[2]**2 < 1.0e-8:
                            glu = x[1]
                            gho = ox[1]
                    #if round(x[4]) == 0 and round(ox[4]) == 1:
                    if abs(x[4]) < 1.0e-8 and ox[4]>0.0:
                        if 0.0 < abs(x[4]) < 1.0:
                            pwarn += 1
                        if x[2] < olu:
                            olu = x[2]
                            klu = [t for t in kt]
                        if ox[2] > oho:
                            oho = ox[2]
                            kho = [t for t in kt]
                        if kt[0]**2 + kt[1]**2 + kt[2]**2 < 1.0e-8:
                            glu = x[2]
                            gho = ox[2]
                    ox = x
        if pwarn > 0 and not quiet:
            print('WARNING:',pwarn,'total values of e(k) with partial occupancies')
        if qone:
            homos['only'] = oho
            gaps['only'] = olu - oho
        else:
            homos[afile[4:len(afile)]] = oho
            gaps[afile[4:len(afile)]] = olu - oho
        kd = 0.0
        for i,u in enumerate(kho):
            kd += (klu[i] - u)**2
        if qone and not quiet:
            if kd < 1.0e-8:
                tgap = 'direct'
            else:
                tgap = 'indirect'
            if olu - oho == 0.0:
                print( 'Gapless')
            elif olu - oho < 1.0e-2:
                print( 'Most likely a metal/semi-metal')
            if olu - oho > 0.0:
                print( 'gap=',olu - oho,'eV,',tgap)
            print( 'E_HO',oho,'eV, at k =',(kho[0],kho[1],kho[2]))
            print( 'E_LU',olu,'eV, at k =',(klu[0],klu[1],klu[2]))
            print(( 'Lowest occupied state {:} eV (occupied bandwidth={:})').format(lo,oho-lo))
    return homos,gaps

if __name__ == '__main__':
    gap()
