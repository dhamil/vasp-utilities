import numpy as np
import os
from read_osz_out import conv_en
import matplotlib.pyplot as plt

def be_curve(osz_l):

    ldr = os.getcwd().split('/')[-1]

    sep_l = []

    en_l = []
    mag_l = []

    tpars = conv_en('./atom/OSZICAR')
    iso = tpars['E']

    n_no_conv = 0
    no_conv_l = []

    for ifl,fl in enumerate(osz_l):
        tpars=conv_en('./outs/'+fl)
        os.system(('grep "NELM" {:} > ./outs/rrrrrrrr.txt').format('./outs/out'+fl[3:]))
        with open('./outs/rrrrrrrr.txt','r') as infl:
            for ln in infl:
                ln = ln.split()
                for ielt,elt in enumerate(ln):
                    if elt == 'NELM':
                        nelm = int(ln[ielt+2].split(';')[0])
        os.system('rm ./outs/rrrrrrrr.txt')

        if tpars['its'] < nelm:

            en_l.append(tpars['E']/2.0-iso)
            sep_l.append(float(fl[4:].split('.txt')[0]))
            iread = -1
            with open('./outs/out'+fl[3:],'r') as infl:
                for iln,ln in enumerate(infl):
                    ln = ln.split()
                    if len(ln)==2:
                        if ln[0] == 'magnetization' and ln[1] == '(x)':
                            iread = iln+4
                    if iln == iread:
                        mag_l.append(abs(float(ln[-1])))
                        break
        else:
            n_no_conv+=1
            no_conv_l.append(fl[4:].split('.txt')[0])

    if n_no_conv > 0:
        print(("WARNING, {:} calculation(s) reached NELM iterations").format(n_no_conv))
        if n_no_conv < 10:
            print(no_conv_l)

    np.savetxt('../'+ldr+'_be_curve.csv',np.transpose((sep_l,en_l,mag_l)),delimiter = ',',header='Sep. (Ang), Energy/atom (eV), Magnetic moment (mu_B)')
    np.savetxt(ldr+'_be_curve.csv',np.transpose((sep_l,en_l,mag_l)),delimiter = ',',header='Sep. (Ang), Energy/atom (eV), Magnetic moment (mu_B)')

    return

if __name__=="__main__":

    tf_l = []
    tf_l2 = []
    for fli in os.listdir(os.getcwd()+'/outs'):
        if fli.startswith('osz_'):
            tf_l.append(fli)
            ttf = fli.split('_')[1]
            tf_l2.append(float(ttf.split('.txt')[0]))
    tf_l = np.asarray(tf_l)
    tf_l = tf_l[np.argsort(tf_l2)]
    be_curve(tf_l)
