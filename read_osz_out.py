from __future__ import print_function
import numpy as np
from os import system,sys

def conv_en(flnm,all_stage=False):

    pars = {}
    # F is Helmholtz free energy, F = E - TS, where finite T (and S) comes from smearing
    # E is energy extrapolated without smearing, E = F + TS
    # dE is change in energy between penultimate and ultimate runs
    # mag, if ISPIN = 2 in the calculation, is the magnetiziation within the calculation cell

    with open(flnm,'r') as infl:
        for iln,ln in enumerate(infl):
            ln = ln.split()
            if all_stage:
                indx = ln[0]
            else:
                indx = flnm
            if ln[1] == 'F=':
                if len((ln[7][1:]).split('E'))==1:
                    dE_temp = 0.0
                else:
                    dE_temp=float(ln[7][1:])
                if len(ln) == 10:
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': float(ln[9]), 'its': int(oln[1])}
                else:
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'its': int(oln[1])}
            oln = ln
    if all_stage:
        return pars
    else:
        return pars[indx]

def get_ispin(flnm):

    system(('grep "ISPIN" {:} > tttmmmppp.txt').format(flnm))
    with open('tttmmmppp.txt','r') as infl:
        for ln in infl:
            ispin = int(ln.split()[2])
            break
    system('rm tttmmmppp.txt')
    return ispin

if __name__=="__main__":

    tpars = conv_en(sys.argv[1])
    print(tpars['F'])
