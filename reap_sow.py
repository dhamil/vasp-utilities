import os
import numpy as np
from math import floor

def sow(nproc,fl_l):

    ndir = min(len(fl_l),nproc)
    nchunk = floor(len(fl_l)/ndir)

    ind_l = [[i*nchunk,(i+1)*nchunk] for i in range(ndir)]
    ind_l[-1][-1] = len(fl_l)

    dir_l = []
    for idir in range(ndir):
        dir_l.append('tmp_'+str(idir))
        os.system(('mkdir tmp_{:}').format(idir))
        os.system(('cp ./POTCAR ./tmp_{:}/').format(idir))
        for ind in range(ind_l[idir][0],ind_l[idir][1]):
            suffix = fl_l[ind][6:]
            os.system(('mv ./{:} ./tmp_{:}/').format(fl_l[ind],idir))
            os.system(('mv ./dos{:} ./tmp_{:}/').format(suffix,idir))
            #os.system(('mv ./eig{:} ./tmp_{:}/').format(suffix,idir))
            os.system(('mv ./osz{:} ./tmp_{:}/').format(suffix,idir))

    return dir_l

def reap(dir_l):

    os.system('touch ks_pot_full.csv')

    dir_l = np.asarray(dir_l)
    indx = np.asarray([x[4:] for x in dir_l])
    dir_l = dir_l[np.argsort(indx)]

    for idir,dir in enumerate(dir_l):
        for fli in os.listdir(os.getcwd()+'/'+dir):
            if fli != 'POSCAR' and fli != 'ks_pot.csv':
                os.system(('mv {:}/{:} .').format(dir,fli))
            elif fli == 'ks_pot.csv':
                if idir == 0:
                    os.system(('cat {:}/ks_pot.csv >> ks_pot_full.csv').format(dir))
                else:
                    dat = np.genfromtxt('./'+dir+'/ks_pot.csv',delimiter=',',skip_header=1)
                    np.savetxt('./'+dir+'/ks_pot2.csv',dat,delimiter=',')
                    os.system(('cat {:}/ks_pot2.csv >> ks_pot_full.csv').format(dir))
                os.system(('mv {:}/ks_pot.csv ./ks_pot_{:}.csv').format(dir,dir[4:]))
        os.system(('rm -rf {:}').format(dir))

    return

if __name__=="__main__":

    """
    tf_l = []
    tf_l2 = []
    for fli in os.listdir(os.getcwd()):
        if fli.startswith('dos_'):
            tf_l.append(fli)
            ttf = fli.split('_')[1]
            tf_l2.append(float(ttf.split('.txt')[0]))
    tf_l = np.asarray(tf_l)
    tf_l = tf_l[np.argsort(tf_l2)]
    sow(int(os.sys.argv[1]),tf_l)
    """
    reap(['tmp_0','tmp_1'])
